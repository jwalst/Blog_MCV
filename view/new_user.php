<?php include '../includes/sentry.php'; ?>
<?php include '../includes/top.php'; ?>
<body>
<?php include '../includes/header.php'; ?>
<?php include '../includes/menu.php'; ?>
<div id="content" class="mdl-layout__content col-md-9">
    <div class="mdl-cell mdl-cell--2-offset-desktop mdl-cell--8-col mdl-cell--4-col-phone">
        <form action="../controller/UserController.php" method="post">
            <h2>Nieuwe gebruiker toevoegen user</h2>
            <br><br>
            <input type="hidden" name="action" value="saveUser">
            Firstname:<br> <input name="firstname" type="text" value="" required=""/><br/>
            Lastname:<br> <input name="lastname" type="text" value=""/><br/>
            Phonenumber:<br> <input name="phonenumber" type="text" value=""/><br/>
            Password:<br> <input name="password" type="password" value="" required=""/><br/>
            <button class="submit" type="submit">Gebruiker updaten</button>
        </form>
    </div>
</div>
</body>
</html>