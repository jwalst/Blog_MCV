<?php session_start(); ?>
<?php include '../includes/top.php'; ?>
    <body>
    <?php include '../includes/header.php'; ?>
    <?php include '../includes/menu.php'; ?>
        <div id="content" class="mdl-layout__content col-md-9">
            <div class="mdl-grid">
                <div class="mdl-cell mdl-cell--2-offset-desktop mdl-cell--8-col mdl-cell--4-col-phone">
                    <h2>Berichten</h2>
                    <?php

                    require_once("../classes/model/Message.php");

                    if (isset($_SESSION['messageList'])) {

                        $messageList = unserialize($_SESSION['messageList']);

                        foreach ($messageList as $message) {
                            echo "<font color=\"blue\"><h4>" . $message->getTitle()."</font></h4>";
                            echo  $message->getContent();
                            echo "<br/><br/>";
                            echo " <a href='../controller/messageController.php?id=". $message->getId()."&&action=findMessageById'>
				    <img src=\"images/select.png\" width=\"50\" height=\"50\"></a><br></br>";
                        }
                    }

                    ?>
                </div>
            </div>
      </div>    
    </body>
</html>