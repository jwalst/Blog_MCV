<?php include '../includes/sentry.php'; ?>
<?php include '../includes/top.php'; ?>
<body>
<?php include '../includes/header.php'; ?>
<?php include '../includes/menu.php'; ?>
<div id="content" class="mdl-layout__content col-md-9">
    <div class="mdl-cell mdl-cell--2-offset-desktop mdl-cell--8-col mdl-cell--4-col-phone">
        <form action="../controller/userController.php" method="GET">
            <?php require_once("../classes/model/User.php");
            $user = unserialize($_SESSION['user']);
            ?>
            <h2>Wilt u deze user verwijderen</h2>
            <br><br>

            <b>Name:</b> <?php echo $user->getFirstname(); ?> <?php echo $user->getLastname(); ?> <br>

            <p>
                <input type="hidden" name="id" value="<?php echo $user->getId(); ?>">
                <button type="submit" name="action" value="JA">Ja</button>
                <button type="submit" name="action" value="NEE">Nee</button>
            </p>


        </form>
    </div>
</div>
</body>
</html>