<?php session_start(); ?>
<?php include '../includes/top.php'; ?>

<body>
<?php include '../includes/header.php'; ?>
<?php include '../includes/menu.php'; ?>
<div id="content" class="mdl-layout__content col-md-9">
    <div class="mdl-cell mdl-cell--2-offset-desktop mdl-cell--8-col mdl-cell--4-col-phone">
        <h2>Login:</h2>
        <table class="login">
            <form action="../controller/userController.php" method="post">
                <input name="action" type="hidden" value="login"/>
                <tr>
                    <td><p>Voornaam:</td>
                    <td><input name="firstname" type="text"/></td>
                </tr>
                </p>
                <tr>
                    <td><p>Wachtwoord:</td>
                    <td><input name="password" type="password"/></td>
                </tr>
                </p>

                <p>
                    <tr>
                        <td></td>
                        <td>
                            <button class="submit" type="submit">Inloggen</button>
                        </td>
                    </tr>
                </p>
        </table>
        </form>
    </div>
</div>
</body>
</html>