<?php session_start(); ?>
<?php include '../includes/top.php'; ?>

<body>
<?php include '../includes/header.php'; ?>
<?php include '../includes/menu.php'; ?>
<div id="content" class="mdl-layout__content col-md-9">
    <div class="mdl-cell mdl-cell--2-offset-desktop mdl-cell--8-col mdl-cell--4-col-phone">
        <?php
        $login = unserialize($_SESSION['login']);

        if ($login == 1) {
            $_SESSION['admin'] = 2;
            header('Location: ../view/homepage.php');
        }
        if ($login != 1) {
            session_destroy();
            header('Location: ../view/error_login.php');
        }


        echo $login;
        ?>
    </div>
</div>
</body>
</html>