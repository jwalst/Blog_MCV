<?php

/**
 * Description of Person @author Peter/Sjoerd
 */
class Message {
    
    private $id;
    private $title;
    private $content;
 
    public function Message($id,$title, $content) {

        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
    }

    public function getId() {
        return $this->id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getContent() {
        return $this->content;
    }


}
