<?php

/**
 * Created by PhpStorm.
 * User: Jeffrey
 * Date: 24-Mar-17
 * Time: 15:57
 */
class Persoon
{
    private $id;
    private $functie;
    private $voornaam;
    private $achternaam;
    private $telefoon;

    public function __construct($id, $functie,$voornaam,$achternaam, $telefoon){

        $this->id = $id;
        $this->functie = $functie;
        $this->voornaam = $voornaam;
        $this->achternaam = $achternaam;
        $this->telefoon = $telefoon;


    }

    public function getUser(){
        return "Hallo ".$this->functie." ".$this->voornaam." ". $this->achternaam. " Uw telefoon nummer is: ".$this->telefoon;
    }

}