<?php
session_start();

if (!isset($_SESSION['name']) || !isset($_SESSION['password'])) {

    die (header('Location: ../view/error_login.php'));


} else {

    require_once("../classes/db/QueryManager.php");
    require_once("../classes/model/User.php");

    $q = new Querymanager();

    if (isset($_SESSION['name']) && isset($_SESSION['password'])) {
        $firstname = $_SESSION['name'];
        $password = $_SESSION['password'];

        $login = $q->loginUser($firstname, $password);

        $_SESSION['login'] = serialize($login);
        $_SESSION['name'] = $firstname;
        $_SESSION['password'] = $password;

    }
}

$login = unserialize($_SESSION['login']);
if ($login != 1 || $login > 1) {

    session_destroy();
    die (header('Location: ../view/error_login.php'));
}

?>