<?php
    session_start();
    require_once("../classes/db/QueryManager.php");
    require_once("../classes/model/Message.php");
	
			
    $q = new Querymanager();

	
	/***************************************************
	 Save message
	***************************************************/
    if (isset($_POST['title']) && isset($_POST['content'])&&($_POST['action']=='saveMessage')) {
        
        $title=$_POST['title'];
        $content=$_POST['content'];
		$userid = $_SESSION['id'];
        $q->saveMessage($title,$content,$userid);
		header('Location: ../index.php'); 
    }
	
	/***************************************************
	 Show messages:
	***************************************************/
	
   if (($_GET['action']=='findAllMessages')&&($_SESSION['admin']==2)) {
    $messageList = $q->findAllMessages();
    $_SESSION['messageList'] = serialize($messageList);
    header('Location: ../view/all_messages_admin.php');
	}
	
	if (($_GET['action']=='findAllMessages')&&($_SESSION['admin']==0)) {
    $messageList = $q->findAllMessages();
    $_SESSION['messageList'] = serialize($messageList);
    header('Location: ../view/all_messages.php');
	}
	
	if (isset($_GET['id'])&&($_GET['action']=='findMessageById')) {
	$id = $_GET['id'];
	$message = $q->findMessageById($id); 
	$_SESSION['message'] = serialize($message);
    header('Location: ../view/one_message.php'); 
        	
	}
	
	/***************************************************
	 Delete message:
	***************************************************/

    if (isset($_GET['id'])&&($_GET['action']=='deleteMessageById')) {
    $id = $_GET['id'];
    $message = $q->findMessageById($id);
    $_SESSION['message'] = serialize($message);
    header('Location: ../view/delete_message.php?id='.$id);

    }

	if (isset($_GET['id'])&&($_GET['action']=='JA')) {
		$id = $_GET['id'];
        $message = $q->deleteMessage($id);
        header('Location: ../index.php');
	}
	if (isset($_GET['id'])&&($_GET['action']=='NEE')) {
		header('Location: ../index.php'); 	
	}
	
	/***************************************************
	 Update message:
	***************************************************/
	if (isset($_POST['id'])&&($_POST['action']=='update')) {
        $id = $_POST['id'];
        $title = $_POST['title'];
        $content = $_POST['content'];
        $message = $q->updateMessage($id, $title, $content);
		header('Location: ../index.php'); 
	}
	if (isset($_GET['id'])&&($_GET['action']=='updateForm')) {
		$id = $_GET['id'];
		$message = $q->findMessageById($id); 
		$_SESSION['message'] = serialize($message);
		header('Location: ../view/update_message.php'); 
	}
    ?>


</body>
</html>